# 
# Project variables
# 
variable "gcp_project" {
    type = "string"
}

variable "gcp_region" {
    type = "string"
}

variable "gcp_credentials" {
    type = "string"
}

variable "cluster_name" {
    type = "string"
}

variable "cluster_location" {
    type = "string"
}

variable "cluster_version" {
    type = "string"
}

variable "cluster_username" {
    type = "string"
}

variable "cluster_password" {
    type = "string"
}
