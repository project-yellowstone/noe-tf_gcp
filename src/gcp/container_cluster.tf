# 
# Main K8S cluster
# 
resource "google_container_cluster" "primary" {
  name               = "${var.cluster_name}"
  location           = "${var.cluster_location}"
  min_master_version = "${var.cluster_version}"

  # We can't create a cluster with no node pool defined, but we want to only use separately managed node pools. So we create the smallest possible default node pool and immediately delete it.
  remove_default_node_pool  = true
  initial_node_count        = 1

  logging_service     = "logging.googleapis.com/kubernetes"
  monitoring_service  = "monitoring.googleapis.com/kubernetes"

  master_auth {
    username  = "${var.cluster_username}"
    password  = "${var.cluster_password}"
    
    client_certificate_config {
      issue_client_certificate = false
    }
  }

  maintenance_policy {
    daily_maintenance_window {
      start_time = "03:00"
    }
  }

  node_config {
    disk_size_gb  = "10"
    disk_type     = "pd-standard"
    preemptible   = false
    machine_type  = "g1-small"
    image_type    = "COS_CONTAINERD"
  }

  lifecycle {
    ignore_changes = ["initial_node_count", "node_pool"]
  }

  timeouts {
    create = "30m"
    update = "30m"
    delete = "30m"
  }
}

resource "google_container_node_pool" "services" {
  name                 = "services"
  location             = "${google_container_cluster.primary.location}"
  cluster              = "${google_container_cluster.primary.name}"
  initial_node_count   = 3

  autoscaling {
    min_node_count = 3
    max_node_count = 10
  }

  management {
    auto_repair  = true
    auto_upgrade = true
  }

  node_config {
    disk_size_gb  = "10"
    disk_type     = "pd-standard"
    preemptible   = false
    machine_type  = "g1-small"
    image_type    = "COS_CONTAINERD"

    metadata = {
      disable-legacy-endpoints = "true"
    }

    oauth_scopes = [
      "https://www.googleapis.com/auth/logging.write",
      "https://www.googleapis.com/auth/monitoring",
    ]
  }

  lifecycle {
    ignore_changes = ["initial_node_count"]
  }

  timeouts {
    create = "30m"
    update = "30m"
    delete = "30m"
  }
}

# resource "google_container_node_pool" "databases" {}

#
# Output for K8S
#
output "client_certificate" {
  value         = "${base64decode(google_container_cluster.primary.master_auth.0.client_certificate)}"
  description   = "K8s cluster certificate."
  sensitive     = true
}

output "client_key" {
  value         = "${base64decode(google_container_cluster.primary.master_auth.0.client_key)}"
  description   = "K8s cluster client encryption key."
  sensitive     = true
}

output "cluster_ca_certificate" {
  value         = "${base64decode(google_container_cluster.primary.master_auth.0.cluster_ca_certificate)}"
  description   = "K8s cluster CA certificate."
  sensitive     = true
}

output "pool_services_id" {
  value         = "${google_container_node_pool.services.id}"
  description   = "K8s pool 'services'."
}

output "host" {
  value         = "${google_container_cluster.primary.endpoint}"
  description   = "K8s cluster hostname."
}
