# 
# Global variables
# 
variable "project" {
  type = "string"
  default = "noe-yellowstone-eorijf"
}

variable "region" {
  type = "string"
  default = "eu-west3-b"
}

variable "credentials" {
  type = "string"
  default = "./credentials-gcp-noe_yellowstone_eorijf.json"
}

variable "cluster_name" {
  type = "string"
  default = "default"
}

variable "cluster_location" {
  type = "string"
  default = "europe-west3-b"
}

variable "cluster_version" {
  type = "string"
  default = "1.13.7-gke.8"
}

variable "k8s_username" {
  type = "string"
  default = "admin"
}

variable "k8s_password" {
  type = "string"
}

# 
# Backend
# 
terraform {
  backend "gcs" {
   bucket  = "terraform-epvbfk"
   prefix  = "terraform/state"
   credentials = "./credentials-gcp-noe_yellowstone_eorijf.json"
  }
}

#
# Modules
#
module "gcp" {
  source            = "./gcp"
  gcp_project       = "${var.project}"
  gcp_region        = "${var.region}"
  gcp_credentials   = "${var.credentials}"
  cluster_name      = "${var.cluster_name}"
  cluster_location  = "${var.cluster_location}"
  cluster_version   = "${var.cluster_version}"
  cluster_username  = "${var.k8s_username}"
  cluster_password  = "${var.k8s_password}"
}

module "k8s" {
  source                      = "./k8s"
  k8s_username                = "${var.k8s_username}"
  k8s_password                = "${var.k8s_password}"
  k8s_client_certificate      = "${module.gcp.client_certificate}"
  k8s_client_key              = "${module.gcp.client_key}"
  k8s_cluster_ca_certificate  = "${module.gcp.cluster_ca_certificate}"
  k8s_host                    = "${module.gcp.host}"
  gcp_pool_services_id        = "${module.gcp.pool_services_id}"
}
