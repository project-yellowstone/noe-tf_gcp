# 
# K8S services
# 
resource "kubernetes_service" "noe-client" {
  metadata {
    name = "noe-client"

    labels = {
      app  = "noe-client"
      tier = "frontend"
    }
  }

  spec {
    type = "LoadBalancer"

    selector = {
      app  = "noe-client"
      tier = "frontend"
    }

    port {
      protocol    = "TCP"
      port        = 80
      target_port = 8080
    }
  }
}

resource "kubernetes_service" "noe-server" {
  metadata {
    name = "noe-server"

    labels = {
      app  = "noe-server"
      tier = "frontend"
    }
  }

  spec {
    type = "LoadBalancer"

    selector = {
      app  = "noe-server"
      tier = "frontend"
    }

    port {
      protocol    = "TCP"
      port        = 80
      target_port = 8080
    }
  }
}

output "service-noe_client-ip" {
  value     = "${kubernetes_service.noe-client.load_balancer_ingress.0.ip}"
  description = "K8s service IP for 'noe-client'."
}

output "service-noe_server-ip" {
  value     = "${kubernetes_service.noe-server.load_balancer_ingress.0.ip}"
  description = "K8s service IP for 'noe-server'."
}
