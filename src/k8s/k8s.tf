# 
# K8S provider
# 
provider "kubernetes" {
  host     = "${var.k8s_host}"

  username = "${var.k8s_username}"
  password = "${var.k8s_password}"

  client_certificate      = "${var.k8s_client_certificate}"
  client_key              = "${var.k8s_client_key}"
  cluster_ca_certificate  = "${var.k8s_cluster_ca_certificate}"
}
