# 
# K8S pods
# 
resource "kubernetes_deployment" "noe-client" {
  metadata {
    name = "noe-client"

    labels = {
      app  = "noe-client"
      tier = "frontend"
    }
  }

  spec {
    replicas = 3

    selector {
      match_labels = {
        app  = "noe-client"
        tier = "frontend"
      }
    }

    strategy {
      type = "RollingUpdate"

      rolling_update {
        max_surge       = "50%"
        max_unavailable = "0"
      }
    }

    template {
      metadata {
        labels = {
          app  = "noe-client"
          tier = "frontend"
        }
      }

      spec {
        restart_policy = "Always"
        
        image_pull_secrets {
          name = "${kubernetes_secret.repository-token-gitlab-noe-client.metadata.0.name}"
        }

        volume {
          name = "build-out"
          empty_dir {
            medium = "Memory"
          }
        }

        volume {
          name = "build-cache"
          empty_dir {}
        }

        container {
          image = "registry.gitlab.com/project-yellowstone/noe-client:latest"
          name  = "noe-client"
          
          volume_mount {
            name = "build-out"
            mount_path = "/usr/src/app/build"
          }

          volume_mount {
            name = "build-cache"
            mount_path = "/usr/src/app/node_modules/.cache"
          }

          env {
            name = "NODE_CONFIG"
            value_from {
              config_map_key_ref {
                key  = "production.json"
                name = "${kubernetes_config_map.config-noe-client.metadata.0.name}"
              }
            }
          }
          
          env {
            name = "APP_ROOT_URL"
            value = "http://${kubernetes_service.noe-client.load_balancer_ingress.0.ip}"
          }

          env {
            name = "API_ROOT_URL"
            value = "http://${kubernetes_service.noe-server.load_balancer_ingress.0.ip}"
          }
          
          env {
            name = "PORT"
            value = "${kubernetes_service.noe-client.spec.0.port.0.target_port}"
          }

          port {
            container_port = 8080
          }

          resources {
            limits {
              cpu    = "400m"
              memory = "600Mi"
            }

            requests {
              cpu    = "50m"
              memory = "50Mi"
            }
          }
        }
      }
    }
  }

  depends_on = ["null_resource.gcp_pool_services"]
}

resource "kubernetes_deployment" "noe-server" {
  metadata {
    name = "noe-server"

    labels = {
      app  = "noe-server"
      tier = "frontend"
    }
  }

  spec {
    replicas = 3

    selector {
      match_labels = {
        app  = "noe-server"
        tier = "frontend"
      }
    }

    strategy {
      type = "RollingUpdate"
      
      rolling_update {
        max_surge       = "50%"
        max_unavailable = "0"
      }
    }

    template {
      metadata {
        labels = {
          app  = "noe-server"
          tier = "frontend"
        }
      }

      spec {
        restart_policy = "Always"

        image_pull_secrets {
          name = "${kubernetes_secret.repository-token-gitlab-noe-server.metadata.0.name}"
        }

        container {
          image = "registry.gitlab.com/project-yellowstone/noe-server:latest"
          name  = "noe-server"
          
          env {
            name = "NODE_CONFIG"
            value_from {
              config_map_key_ref {
                key  = "production.json"
                name = "${kubernetes_config_map.config-noe-server.metadata.0.name}"
              }
            }
          }
          
          env {
            name = "PORT"
            value = "${kubernetes_service.noe-server.spec.0.port.0.target_port}"
          }

          port {
            container_port = 8080
          }

          resources {
            limits {
              cpu    = "100m"
              memory = "100Mi"
            }

            requests {
              cpu    = "50m"
              memory = "50Mi"
            }
          }
        }
      }
    }
  }

  depends_on = ["null_resource.gcp_pool_services"]
}
