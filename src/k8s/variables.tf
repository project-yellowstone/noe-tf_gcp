# 
# K8S variables
# 
variable "k8s_username" {
  type = "string"
}

variable "k8s_password" {
  type = "string"
}

variable "k8s_client_certificate" {
  type = "string"
}

variable "k8s_client_key" {
  type = "string"
}

variable "k8s_cluster_ca_certificate" {
  type = "string"
}

variable "k8s_host" {
  type = "string"
}

variable "gcp_pool_services_id" {
  type = "string"
}

#
# Dependencies
#
resource "null_resource" "gcp_pool_services" {
  triggers = {
    vpc_name = "${var.gcp_pool_services_id}"
  }
}

# 
# Secrets
# 
resource "kubernetes_secret" "repository-token-gitlab-noe-client" {
  metadata {
    name = "repository-token-gitlab-noe-client"
  }

  data = {
    ".dockerconfigjson" = "${file("./credentials-docker_registry-noe_client.json")}"
  }

  type = "kubernetes.io/dockerconfigjson"
}

resource "kubernetes_secret" "repository-token-gitlab-noe-server" {
  metadata {
    name = "repository-token-gitlab-noe-server"
  }

  data = {
    ".dockerconfigjson" = "${file("./credentials-docker_registry-noe_server.json")}"
  }

  type = "kubernetes.io/dockerconfigjson"
}

resource "kubernetes_config_map" "config-noe-client" {
  metadata {
    name = "config-noe-client"
  }

  data = {
    "production.json" = "${file("./config-noe_client.json")}"
  }
}

resource "kubernetes_config_map" "config-noe-server" {
  metadata {
    name = "config-noe-server"
  }

  data = {
    "production.json" = "${file("./config-noe_server.json")}"
  }
}
