export TF_VAR_billing_account=01B57E-02AF3F-874F30
export TF_PROJECT=noe-yellowstone-eorijf
export TF_ADMIN=terraform-admin
export TF_CREDS=terraform-admin.json

gcloud projects create ${TF_PROJECT} \
  --set-as-default

gcloud beta billing projects link ${TF_PROJECT} \
  --billing-account ${TF_VAR_billing_account}

gcloud iam service-accounts create terraform \
  --display-name "Terraform admin account"

gcloud iam service-accounts keys create ${TF_PROJECT} \
  --iam-account terraform@${TF_PROJECT}.iam.gserviceaccount.com

gcloud projects add-iam-policy-binding ${TF_PROJECT} \
  --member serviceAccount:terraform@${TF_PROJECT}.iam.gserviceaccount.com \
  --role roles/viewer

gcloud projects add-iam-policy-binding ${TF_PROJECT} \
  --member serviceAccount:terraform@${TF_PROJECT}.iam.gserviceaccount.com \
  --role roles/storage.admin

gcloud services enable cloudresourcemanager.googleapis.com
gcloud services enable cloudbilling.googleapis.com
gcloud services enable iam.googleapis.com
gcloud services enable compute.googleapis.com

gsutil mb -p ${TF_PROJECT} gs://${TF_ADMIN}

gsutil versioning set on gs://${TF_ADMIN}

export GOOGLE_APPLICATION_CREDENTIALS=${TF_CREDS}
export GOOGLE_PROJECT=${TF_PROJECT}
